var $tw = require("./boot/boot.js").TiddlyWiki();

var $dir = process.env.OPENSHIFT_DATA_DIR + "wikis"

$tw.boot.argv = [
  $dir,
  "--verbose",
  "--server",
  process.env.OPENSHIFT_NODEJS_PORT,
  "$:/core/save/all",
  "text/plain",
  "text/html",
  "dan",
  "test",
  process.env.OPENSHIFT_NODEJS_IP,
];

$tw.boot.boot();